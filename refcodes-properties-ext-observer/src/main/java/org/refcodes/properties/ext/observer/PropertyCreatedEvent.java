// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.struct.Property;

/**
 * The {@link PropertyCreatedEvent} is fired whenever a property is created in
 * the related {@link ObservableProperties} instance (as of
 * {@link PropertiesBuilder#put(Object, String)} or the like). A
 * {@link PropertyCreatedEvent} represents a {@link Property} with a key and a
 * value (as of {@link Property#getKey()} and {@link Property#getValue()}) of
 * the created property.
 */
public class PropertyCreatedEvent extends PropertyEvent {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link PropertyCreatedEvent} with the given key and value
	 * representing the affected {@link Property}.
	 * 
	 * @param aProperty The key and value pair of the {@link Property} being
	 *        involved in the event.
	 * @param aSource The source from which this event originated.
	 */
	public PropertyCreatedEvent( Property aProperty, ObservableProperties aSource ) {
		super( aProperty, PropertyAction.PROPERTY_CREATED, aSource );
	}

	/**
	 * Constructs a {@link PropertyCreatedEvent} with the given key and value
	 * representing the affected {@link Property}.
	 * 
	 * @param aKey The key of the {@link Property} being involved in the event.
	 * @param aValue The value of the {@link Property} being involved in the
	 *        event.
	 * @param aSource The source from which this event originated.
	 */
	public PropertyCreatedEvent( String aKey, String aValue, ObservableProperties aSource ) {
		super( aKey, aValue, PropertyAction.PROPERTY_CREATED, aSource );
	}
}
