// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;

/**
 * Makes a {@link PropertiesBuilder} instance observable. The decorator provided
 * the additional {@link ObservablePropertiesBuilder} functionality to the
 * decorated {@link PropertiesBuilder}.
 */
public class ObservableResourcePropertiesBuilderDecorator extends AbstractObservableResourcePropertiesBuilderDecorator<ResourcePropertiesBuilder> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link ObservableResourcePropertiesBuilderDecorator} by
	 * decorating the provided {@link ResourcePropertiesBuilder} with observable
	 * functionality.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} to be decorated with
	 *        {@link ObservableResourcePropertiesBuilder} functionality.
	 */
	public ObservableResourcePropertiesBuilderDecorator( ResourcePropertiesBuilder aProperties ) {
		super( aProperties );
	}
}
