// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import java.util.Collection;

import org.refcodes.observer.Observable;
import org.refcodes.properties.Properties;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * The {@link ObservableProperties} represent {@link Properties} which are
 * observable in terms of create, update and delete operations: Create: Whenever
 * a property is created (as of {@link PropertiesBuilder#put(Object, Object)})
 * in the related {@link ObservableProperties} instance, then the
 * {@link PropertiesObserver#onPropertyCreatedEvent(PropertyCreatedEvent)}
 * method of a registered {@link PropertiesObserver} instance is invoked.
 * Update: Whenever an existing property (as of
 * {@link Properties#containsKey(Object)}) is updated in the related
 * {@link ObservableProperties} instance (as of
 * {@link PropertiesBuilder#put(Object, Object)} or the like}, then the
 * {@link PropertiesObserver#onPropertyUpdatedEvent(PropertyUpdatedEvent)}
 * method of a registered {@link PropertiesObserver} instance is invoked.
 * Delete: Whenever a property is deleted (as of
 * {@link PropertiesBuilder#remove(Object)}), then the
 * {@link PropertiesObserver#onPropertyDeletedEvent(PropertyDeletedEvent)}
 * method of a registered {@link PropertiesObserver} instance is invoked. A
 * catch-all (lambda) for all events is provided by the
 * {@link PropertiesObserver} via the
 * {@link PropertiesObserver#onEvent(PropertyEvent)} method. You can provide a
 * lambda expression when calling {@link #subscribeObserver(Object)}, thereby
 * implementing this catch-all method.
 */
public interface ObservableProperties extends Properties, Observable<PropertiesObserver> {

	/**
	 * The {@link ObservableMutableProperties} represent mutable
	 * {@link ObservableProperties}.
	 */
	public interface ObservableMutableProperties extends MutableProperties, ObservableProperties {}

	/**
	 * The {@link ObservablePropertiesBuilder} represents a
	 * {@link ObservableMutableProperties} builder.
	 */
	public interface ObservablePropertiesBuilder extends PropertiesBuilder, ObservableMutableProperties {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPut( Collection<?> aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPut( Object[] aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPut( Relation<String, String> aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPut( String aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default ObservablePropertiesBuilder withPut( Object aPath, String aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPut( Property aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPut( String[] aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutBoolean( Object aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutBoolean( String aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutByte( Object aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutByte( String aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutByte( String[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutChar( Object aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutChar( Object[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutChar( String aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutChar( String[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservablePropertiesBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservablePropertiesBuilder withPutClass( Object aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservablePropertiesBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservablePropertiesBuilder withPutClass( String aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservablePropertiesBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDouble( Object aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDouble( String aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDouble( String[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservablePropertiesBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservablePropertiesBuilder withPutEnum( Object aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservablePropertiesBuilder withPutEnum( Object[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservablePropertiesBuilder withPutEnum( String aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservablePropertiesBuilder withPutEnum( String[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutFloat( Object aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutFloat( String aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutFloat( String[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutInt( Object aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutInt( String aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutInt( String[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutLong( Object aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutLong( Object[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutLong( String aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutLong( String[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutShort( Object aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutShort( Object[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutShort( String aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutShort( String[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutString( Collection<?> aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutString( Object aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutString( Object[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutString( String aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutString( String[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsert( PathMap<String> aFrom ) {
			insert( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( Object aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( String aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMerge( PathMap<String> aFrom ) {
			merge( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( Object aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( String aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( int aIndex, PathMap<String> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservablePropertiesBuilder withRemovePaths( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}
}
