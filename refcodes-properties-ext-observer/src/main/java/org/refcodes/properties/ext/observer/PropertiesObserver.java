// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import org.refcodes.observer.Observer;
import org.refcodes.properties.Properties;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.struct.Property;

/**
 * Defines an observer to be registered to an {@link ObservableProperties} (or
 * one of its sub-classes) instance via
 * {@link ObservableProperties#subscribeObserver(Object)}.
 */
@FunctionalInterface
public interface PropertiesObserver extends Observer<PropertyEvent> {

	/**
	 * This is a catch-all (lambda) method for all events. You can provide a
	 * lambda expression when calling
	 * {@link ObservableProperties#subscribeObserver(Object)}, thereby
	 * implementing this catch-all method. Whenever a property changes (create,
	 * update, delete) in the related {@link ObservableProperties} instance,
	 * then this method is invoked on the listener. The {@link PropertyAction}
	 * accessible via {@link PropertyEvent#getAction()} details the operation in
	 * question.
	 * 
	 * @param aEvent An event implementing the {@link PropertyEvent} e.g.
	 *        {@link PropertyCreatedEvent}, {@link PropertyUpdatedEvent},
	 *        {@link PropertyDeletedEvent}.
	 */
	@Override
	void onEvent( PropertyEvent aEvent );

	/**
	 * Whenever a property is created (as of
	 * {@link PropertiesBuilder#put(Object, Object)}) in the related
	 * {@link ObservableProperties} instance, then this method is invoked on the
	 * listener.
	 * 
	 * @param aEvent A {@link PropertyCreatedEvent} representing the
	 *        {@link Property} which has been created (as of
	 *        {@link PropertyCreatedEvent#getKey()} and
	 *        {@link PropertyCreatedEvent#getValue()})
	 */
	default void onPropertyCreatedEvent( PropertyCreatedEvent aEvent ) {}

	/**
	 * Whenever an existing property (as of
	 * {@link Properties#containsKey(Object)}) is updated in the related
	 * {@link ObservableProperties} instance (as of
	 * {@link PropertiesBuilder#put(Object, Object)} or the like), then this
	 * method is invoked on the listener.
	 * 
	 * @param aEvent A {@link PropertyUpdatedEvent} representing the
	 *        {@link Property} which has been updated (as of
	 *        {@link PropertyUpdatedEvent#getKey()},
	 *        {@link PropertyUpdatedEvent#getValue()} and
	 *        {@link PropertyUpdatedEvent#getPreviousValue()}).
	 */
	default void onPropertyUpdatedEvent( PropertyUpdatedEvent aEvent ) {}

	/**
	 * Whenever a property is deleted (as of
	 * {@link PropertiesBuilder#remove(Object)}) in the related
	 * {@link ObservableProperties} instance, then this method is invoked on the
	 * listener.
	 * 
	 * @param aEvent A {@link PropertyDeletedEvent} representing the
	 *        {@link Property} which has been removed (as of
	 *        {@link PropertyDeletedEvent#getKey()} and
	 *        {@link PropertyDeletedEvent#getValue()})
	 */
	default void onPropertyDeletedEvent( PropertyDeletedEvent aEvent ) {};

}
