// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Collection;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.properties.Properties;
import org.refcodes.properties.PropertiesImpl;
import org.refcodes.properties.ReloadMode;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.ext.observer.ObservableResouceProperties.ObservableResourcePropertiesBuilder;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

public abstract class AbstractObservableResourcePropertiesBuilderDecorator<T extends ResourcePropertiesBuilder> extends AbstractObservablePropertiesBuilderDecorator<T> implements ObservableResourcePropertiesBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	protected AbstractObservableResourcePropertiesBuilderDecorator( T aProperties ) {
		super( aProperties );
	}

	protected AbstractObservableResourcePropertiesBuilderDecorator( T aProperties, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		super( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// EVENTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties loadFrom( InputStream aInputStream ) throws IOException, ParseException {
		final Properties theBefore = new PropertiesImpl( (Properties) this );
		final Properties theResult = getProperties().loadFrom( aInputStream );
		fireEvents( theBefore, theResult );
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties seekFrom( Class<?> aResourceClass, String aFilePath, ConfigLocator aConfigLocator ) throws IOException, ParseException {
		final Properties theBefore = new PropertiesImpl( (Properties) this );
		final Properties theResult = getProperties().seekFrom( aResourceClass, aFilePath );
		fireEvents( theBefore, theResult );
		return theResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload() throws IOException, ParseException {
		return reload( ReloadMode.KEEP_ORPHANS );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties reload( ReloadMode aReloadMode ) throws IOException, ParseException {
		final Properties theBefore = new PropertiesImpl( (Properties) this );
		final Properties theResult = getProperties().reload( aReloadMode );
		fireEvents( theBefore, theResult );

		// Orphan removal |-->
		if ( aReloadMode == ReloadMode.ORPHAN_REMOVAL ) {
			PropertyEvent eEvent;
			for ( String eKey : theBefore.keySet() ) {
				if ( !containsKey( eKey ) ) {
					eEvent = new PropertyDeletedEvent( toNormalizedPath( eKey ), theBefore.get( eKey ), this );
					try {
						_propertiesObservable.fireEvent( eEvent );
					}
					catch ( VetoException ignore ) { /* ignore */ }
				}
			}
		}
		// Orphan removal <--|

		return theResult;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toSerialized( char aDelimiter ) {
		return getProperties().toSerialized( aDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public File saveTo( File aFile, String aComment, char aDelimiter ) throws IOException {
		return getProperties().saveTo( aFile, aComment, aDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveTo( OutputStream aOutputStream, String aComment, char aDelimiter ) throws IOException {
		getProperties().saveTo( aOutputStream, aComment, aDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		getProperties().flush();
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveFrom( String aFromPath ) {
	//		return getProperties().retrieveFrom( aFromPath );
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveTo( String aToPath ) {
	//		return getProperties().retrieveTo( aToPath );
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties loadFrom( File aFile ) throws IOException, ParseException {
		return getProperties().loadFrom( aFile );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insert( Object aFrom ) {
		getProperties().insert( aFrom );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertFrom( Object aFrom, String aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertTo( String aToPath, Object aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void merge( Object aFrom ) {
		getProperties().merge( aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeFrom( Object aFrom, String aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeTo( String aToPath, Object aFrom ) {
		getProperties().mergeTo( aToPath, aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPut( Collection<?> aPathElements, String aValue ) {
		getProperties().put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPut( Object[] aPathElements, String aValue ) {
		getProperties().put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPut( Relation<String, String> aProperty ) {
		getProperties().put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPut( String aKey, String aValue ) {
		getProperties().put( aKey, aValue );
		return this;
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public ObservableResourcePropertiesBuilder withPut( Object aPath, String aValue ) {
	//		getProperties().put( toPath( aPath ), aValue );
	//		return this;
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPut( Property aProperty ) {
		getProperties().put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPut( String[] aKey, String aValue ) {
		getProperties().put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		getProperties().putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutBoolean( Object aKey, Boolean aValue ) {
		getProperties().putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		getProperties().putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutBoolean( String aKey, Boolean aValue ) {
		getProperties().putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
		getProperties().putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
		getProperties().putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutByte( Object aKey, Byte aValue ) {
		getProperties().putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
		getProperties().putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutByte( String aKey, Byte aValue ) {
		getProperties().putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutByte( String[] aPathElements, Byte aValue ) {
		getProperties().putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
		getProperties().putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutChar( Object aKey, Character aValue ) {
		getProperties().putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutChar( Object[] aPathElements, Character aValue ) {
		getProperties().putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutChar( String aKey, Character aValue ) {
		getProperties().putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutChar( String[] aPathElements, Character aValue ) {
		getProperties().putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObservableResourcePropertiesBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		getProperties().putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObservableResourcePropertiesBuilder withPutClass( Object aKey, Class<C> aValue ) {
		getProperties().putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObservableResourcePropertiesBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
		getProperties().putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObservableResourcePropertiesBuilder withPutClass( String aKey, Class<C> aValue ) {
		getProperties().putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObservableResourcePropertiesBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
		getProperties().putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
		getProperties().putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDouble( Object aKey, Double aValue ) {
		getProperties().putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
		getProperties().putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDouble( String aKey, Double aValue ) {
		getProperties().putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDouble( String[] aPathElements, Double aValue ) {
		getProperties().putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
		getProperties().putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( Object aKey, E aValue ) {
		getProperties().putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( Object[] aPathElements, E aValue ) {
		getProperties().putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( String aKey, E aValue ) {
		getProperties().putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( String[] aPathElements, E aValue ) {
		getProperties().putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
		getProperties().putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutFloat( Object aKey, Float aValue ) {
		getProperties().putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
		getProperties().putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutFloat( String aKey, Float aValue ) {
		getProperties().putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutFloat( String[] aPathElements, Float aValue ) {
		getProperties().putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
		getProperties().putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutInt( Object aKey, Integer aValue ) {
		getProperties().putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
		getProperties().putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutInt( String aKey, Integer aValue ) {
		getProperties().putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutInt( String[] aPathElements, Integer aValue ) {
		getProperties().putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
		getProperties().putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutLong( Object aKey, Long aValue ) {
		getProperties().putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutLong( Object[] aPathElements, Long aValue ) {
		getProperties().putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutLong( String aKey, Long aValue ) {
		getProperties().putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutLong( String[] aPathElements, Long aValue ) {
		getProperties().putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
		getProperties().putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutShort( Object aKey, Short aValue ) {
		getProperties().putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutShort( Object[] aPathElements, Short aValue ) {
		getProperties().putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutShort( String aKey, Short aValue ) {
		getProperties().putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutShort( String[] aPathElements, Short aValue ) {
		getProperties().putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutString( Collection<?> aPathElements, String aValue ) {
		getProperties().putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutString( Object aKey, String aValue ) {
		getProperties().putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutString( Object[] aPathElements, String aValue ) {
		getProperties().putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutString( String aKey, String aValue ) {
		getProperties().putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutString( String[] aPathElements, String aValue ) {
		getProperties().putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsert( Object aObj ) {
		getProperties().insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsert( PathMap<String> aFrom ) {
		getProperties().insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		getProperties().insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		getProperties().insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		getProperties().insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		getProperties().insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		getProperties().insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		getProperties().insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		getProperties().insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, String aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
		getProperties().insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		getProperties().insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		getProperties().insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		getProperties().insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		getProperties().insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( Object aToPath, Object aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
		getProperties().insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		getProperties().insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( String aToPath, Object aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( String aToPath, PathMap<String> aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
		getProperties().insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		getProperties().insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMerge( Object aObj ) {
		getProperties().merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMerge( PathMap<String> aFrom ) {
		getProperties().merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		getProperties().mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		getProperties().mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		getProperties().mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		getProperties().mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		getProperties().mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		getProperties().mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		getProperties().mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		getProperties().mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, String aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
		getProperties().mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		getProperties().mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		getProperties().mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		getProperties().mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		getProperties().mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		getProperties().mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( Object aToPath, Object aFrom ) {
		getProperties().mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		getProperties().mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
		getProperties().mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		getProperties().mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( String aToPath, Object aFrom ) {
		getProperties().mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( String aToPath, PathMap<String> aFrom ) {
		getProperties().mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
		getProperties().mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		getProperties().mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		getProperties().putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		getProperties().putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( int aIndex, Object aDir ) {
		getProperties().putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( int aIndex, PathMap<String> aDir ) {
		getProperties().putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		getProperties().putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		getProperties().putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		getProperties().putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		getProperties().putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
		getProperties().putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		getProperties().putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		getProperties().putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		getProperties().putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withRemoveFrom( Collection<?> aPathElements ) {
		getProperties().removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withRemoveFrom( Object aPath ) {
		getProperties().removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withRemoveFrom( Object... aPathElements ) {
		getProperties().removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withRemoveFrom( String aPath ) {
		getProperties().removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withRemoveFrom( String... aPathElements ) {
		getProperties().removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObservableResourcePropertiesBuilder withRemovePaths( String... aPathElements ) {
		getProperties().removeFrom( aPathElements );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// OBSERVABLE:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
