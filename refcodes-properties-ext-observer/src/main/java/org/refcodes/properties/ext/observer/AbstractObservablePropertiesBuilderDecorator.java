// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.ActionEvent;
import org.refcodes.properties.AbstractPropertiesDecorator;
import org.refcodes.properties.Properties;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.ext.observer.ObservableProperties.ObservablePropertiesBuilder;

public abstract class AbstractObservablePropertiesBuilderDecorator<T extends PropertiesBuilder> extends AbstractPropertiesDecorator<T> implements ObservablePropertiesBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected PropertiesObservable _propertiesObservable;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	protected AbstractObservablePropertiesBuilderDecorator( T aProperties ) {
		super( aProperties );
		_propertiesObservable = new PropertiesObservable();
	}

	protected AbstractObservablePropertiesBuilderDecorator( T aProperties, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		super( aProperties );
		_propertiesObservable = new PropertiesObservable( aExecutorService, aExecutionStrategy );
	}

	// /////////////////////////////////////////////////////////////////////////
	// EVENTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String put( String aKey, String aValue ) {
		PropertyEvent theEvent = null;
		final String theBefore;
		if ( containsKey( aKey ) ) {
			theBefore = getProperties().put( aKey, aValue );
			if ( ( theBefore != null && !theBefore.equals( aValue ) ) || ( theBefore != null ) != ( aValue != null ) ) {
				theEvent = new PropertyUpdatedEvent( toNormalizedPath( aKey ), aValue, theBefore, this );
			}
		}
		else {
			theBefore = getProperties().put( aKey, aValue );
			theEvent = new PropertyCreatedEvent( toNormalizedPath( aKey ), aValue, this );
		}
		if ( theEvent != null ) {
			try {

				_propertiesObservable.fireEvent( theEvent );
			}
			catch ( VetoException ignore ) { /* ignore */ }
		}
		return theBefore;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		final Set<String> theKeys = new HashSet<>( keySet() );
		for ( String eKey : theKeys ) {
			remove( eKey );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<Entry<String, String>> entrySet() {
		return getProperties().entrySet();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putAll( Map<? extends String, ? extends String> aMap ) {
		for ( String eKey : aMap.keySet() ) {
			put( eKey, aMap.get( eKey ) );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String remove( Object aKey ) {
		final String theKey = aKey != null ? aKey.toString() : null;
		final String theValue = getProperties().remove( theKey );
		final PropertyEvent theEvent = new PropertyDeletedEvent( toNormalizedPath( theKey ), theValue, this );
		try {
			_propertiesObservable.fireEvent( theEvent );
		}
		catch ( VetoException ignoree ) { /* ignore */ }
		return theValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveFrom( String aFromPath ) {
	//		return getProperties().retrieveFrom( aFromPath );
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveTo( String aToPath ) {
	//		return getProperties().retrieveTo( aToPath );
	//	}

	// /////////////////////////////////////////////////////////////////////////
	// OBSERVABLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasObserver( PropertiesObserver aObserver ) {
		return _propertiesObservable.hasObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( PropertiesObserver aObserver ) {
		return _propertiesObservable.subscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( PropertiesObserver aObserver ) {
		return _propertiesObservable.unsubscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insert( Object aFrom ) {
		getProperties().insert( aFrom );

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().insertBetween( aToPath, aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertFrom( Object aFrom, String aFromPath ) {
		getProperties().insertFrom( aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insertTo( String aToPath, Object aFrom ) {
		getProperties().insertTo( aToPath, aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void merge( Object aFrom ) {
		getProperties().merge( aFrom );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		getProperties().mergeBetween( aToPath, aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeFrom( Object aFrom, String aFromPath ) {
		getProperties().mergeFrom( aFrom, aFromPath );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected void fireEvents( Properties aBefore, Properties aAfter ) {
		PropertyEvent eEvent;
		String ePreviousValue;
		String eValue;
		for ( String eKey : aAfter.keySet() ) {
			eEvent = null;
			if ( !aBefore.containsKey( eKey ) ) {
				eEvent = new PropertyCreatedEvent( toNormalizedPath( eKey ), get( eKey ), this );
			}
			else {
				ePreviousValue = aBefore.get( eKey );
				eValue = get( eKey );
				// Created or updated |-->
				if ( !( ePreviousValue == eValue || ( ePreviousValue != null && ePreviousValue.equals( eValue ) ) ) ) {
					if ( ( ePreviousValue != null && !ePreviousValue.equals( eValue ) ) || ( ePreviousValue != null ) != ( eValue != null ) ) {
						eEvent = new PropertyUpdatedEvent( toNormalizedPath( eKey ), eValue, ePreviousValue, this );
					}
					else {
						eEvent = new PropertyCreatedEvent( toNormalizedPath( eKey ), eValue, this );
					}
				}
				// Created or updated <--|
			}
			if ( eEvent != null ) {
				try {
					_propertiesObservable.fireEvent( eEvent );
				}
				catch ( VetoException ignore ) { /* ignore */ }
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link ConnectionObservable} extends the {@link AbstractObservable}
	 * to make the using class {@link ActionEvent} aware.
	 */
	class PropertiesObservable extends AbstractObservable<PropertiesObserver, PropertyEvent> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final ExecutionStrategy _executionStrategy;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new connection observable.
		 */
		public PropertiesObservable() {
			_executionStrategy = ExecutionStrategy.SEQUENTIAL;
		}

		/**
		 * Instantiates a new connection observable.
		 *
		 * @param aExecutorService the executor service to use when dispatching
		 *        events.
		 * @param aExecutionStrategy the execution strategy to use when
		 *        dispatching events.
		 */
		public PropertiesObservable( ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
			super( aExecutorService );
			_executionStrategy = ( aExecutionStrategy != null ) ? aExecutionStrategy : ExecutionStrategy.SEQUENTIAL;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int size() {
			return super.size();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isEmpty() {
			return super.isEmpty();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void clear() {
			super.clear();
		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Same as {@link #fireEvent(MetaDataEvent<?>, ExecutionStrategy)} with
		 * a predefined {@link ExecutionStrategy#JOIN}.
		 *
		 * @param aEvent the event
		 * 
		 * @return true, if successful
		 * 
		 * @throws VetoException the veto exception
		 * 
		 * @see #fireEvent(MetaDataEvent<?>, ExecutionStrategy)
		 */
		protected boolean fireEvent( PropertyEvent aEvent ) throws VetoException {
			return super.fireEvent( aEvent, _executionStrategy );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected boolean fireEvent( PropertyEvent aEvent, PropertiesObserver aObserver, ExecutionStrategy aExecutionStrategy ) throws VetoException {
			if ( aEvent instanceof PropertyCreatedEvent ) {
				aObserver.onPropertyCreatedEvent( (PropertyCreatedEvent) aEvent );
			}
			else if ( aEvent instanceof PropertyUpdatedEvent ) {
				aObserver.onPropertyUpdatedEvent( (PropertyUpdatedEvent) aEvent );
			}
			else if ( aEvent instanceof PropertyDeletedEvent ) {
				aObserver.onPropertyDeletedEvent( (PropertyDeletedEvent) aEvent );
			}
			aObserver.onEvent( aEvent );
			return true;
		}
	}
}
