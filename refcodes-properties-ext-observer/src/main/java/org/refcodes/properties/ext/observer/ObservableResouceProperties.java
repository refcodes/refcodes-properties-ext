// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import java.util.Collection;

import org.refcodes.properties.ResourceProperties;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * The {@link ObservableResouceProperties} represent {@link ResourceProperties}
 * which are observable in terms of create, update and delete operations:
 * Create: Whenever a property is created (as of
 * {@link PropertiesBuilder#put(Object, Object)}) in the related
 * {@link ObservableResouceProperties} instance, then this method is invoked on
 * the listener. Update: Whenever an existing property (as of
 * {@link ResourceProperties#containsKey(Object)}) is updated in the related
 * {@link ObservableResouceProperties} instance (as of
 * {@link PropertiesBuilder#put(Object, Object)} or the like}, then this method
 * is invoked on the listener. Delete: Whenever a property is deleted (as of
 * {@link PropertiesBuilder#remove(Object)}) in the related
 * {@link ObservableResouceProperties} instance, then this method is invoked on
 * the listener.
 */
public interface ObservableResouceProperties extends ObservableProperties, ResourceProperties {

	/**
	 * The {@link ObservableMutableResourceProperties} represent mutable
	 * {@link ObservableMutableProperties}.
	 */
	public interface ObservableMutableResourceProperties extends ObservableResouceProperties, ObservableMutableProperties, MutableResoureProperties {}

	/**
	 * The {@link ObservableResourcePropertiesBuilder} represents a
	 * {@link ObservableMutableResourceProperties} builder.
	 */
	public interface ObservableResourcePropertiesBuilder extends ResourcePropertiesBuilder, ObservablePropertiesBuilder, ObservableMutableResourceProperties {

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPut( Collection<?> aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPut( Object[] aPathElements, String aValue ) {
			put( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPut( Relation<String, String> aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPut( String aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		//	/**
		//	 * {@inheritDoc}
		//	 */
		//	@Override
		//	default ObservableResourcePropertiesBuilder withPut( Object aPath, String aValue ) {
		//		put( toPath( aPath ), aValue );
		//		return this;
		//	}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPut( Property aProperty ) {
			put( aProperty );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPut( String[] aKey, String aValue ) {
			put( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutBoolean( Object aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutBoolean( String aKey, Boolean aValue ) {
			putBoolean( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
			putBoolean( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutByte( Object aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutByte( String aKey, Byte aValue ) {
			putByte( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutByte( String[] aPathElements, Byte aValue ) {
			putByte( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutChar( Object aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutChar( Object[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutChar( String aKey, Character aValue ) {
			putChar( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutChar( String[] aPathElements, Character aValue ) {
			putChar( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservableResourcePropertiesBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservableResourcePropertiesBuilder withPutClass( Object aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservableResourcePropertiesBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservableResourcePropertiesBuilder withPutClass( String aKey, Class<C> aValue ) {
			putClass( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <C> ObservableResourcePropertiesBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
			putClass( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDouble( Object aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDouble( String aKey, Double aValue ) {
			putDouble( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDouble( String[] aPathElements, Double aValue ) {
			putDouble( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( Object aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( Object[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( String aKey, E aValue ) {
			putEnum( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default <E extends Enum<E>> ObservableResourcePropertiesBuilder withPutEnum( String[] aPathElements, E aValue ) {
			putEnum( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutFloat( Object aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutFloat( String aKey, Float aValue ) {
			putFloat( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutFloat( String[] aPathElements, Float aValue ) {
			putFloat( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutInt( Object aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutInt( String aKey, Integer aValue ) {
			putInt( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutInt( String[] aPathElements, Integer aValue ) {
			putInt( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutLong( Object aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutLong( Object[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutLong( String aKey, Long aValue ) {
			putLong( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutLong( String[] aPathElements, Long aValue ) {
			putLong( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutShort( Object aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutShort( Object[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutShort( String aKey, Short aValue ) {
			putShort( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutShort( String[] aPathElements, Short aValue ) {
			putShort( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutString( Collection<?> aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutString( Object aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutString( Object[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutString( String aKey, String aValue ) {
			putString( aKey, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutString( String[] aPathElements, String aValue ) {
			putString( aPathElements, aValue );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsert( Object aObj ) {
			insert( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsert( PathMap<String> aFrom ) {
			insert( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			insertBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			insertBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			withInsertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
			insertFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			insertFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( Object aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( Object aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( String aToPath, Object aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( String aToPath, PathMap<String> aFrom ) {
			insertTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
			insertTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMerge( Object aObj ) {
			merge( aObj );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMerge( PathMap<String> aFrom ) {
			merge( aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
			mergeBetween( aToPath, aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
			mergeBetween( aToPathElements, aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
			mergeFrom( aFrom, aFromPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
			mergeFrom( aFrom, aFromPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( Object aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( Object aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( String aToPath, Object aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( String aToPath, PathMap<String> aFrom ) {
			mergeTo( aToPath, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
			mergeTo( aToPathElements, aFrom );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( int aIndex, Object aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( int aIndex, PathMap<String> aDir ) {
			putDirAt( aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPath, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
			putDirAt( aPathElements, aIndex, aDir );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withRemoveFrom( Collection<?> aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withRemoveFrom( Object aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withRemoveFrom( Object... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withRemoveFrom( String aPath ) {
			removeFrom( aPath );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withRemoveFrom( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		default ObservableResourcePropertiesBuilder withRemovePaths( String... aPathElements ) {
			removeFrom( aPathElements );
			return this;
		}
	}
}
