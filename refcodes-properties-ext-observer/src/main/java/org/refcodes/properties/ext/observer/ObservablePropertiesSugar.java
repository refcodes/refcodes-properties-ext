// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesSugar;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.ext.observer.ObservableProperties.ObservablePropertiesBuilder;
import org.refcodes.properties.ext.observer.ObservableResouceProperties.ObservableResourcePropertiesBuilder;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the construction of
 * {@link ObservableProperties} (and the like).
 */
public class ObservablePropertiesSugar extends PropertiesSugar {

	// /////////////////////////////////////////////////////////////////////////
	// JAVA PROPERTIES:
	// /////////////////////////////////////////////////////////////////////////

	private ObservablePropertiesSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * Create an {@link ObservablePropertiesBuilder} by decorating the provided
	 * {@link PropertiesBuilder} with observable functionality.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} to be decorated with
	 *        {@link ObservablePropertiesBuilder} functionality.
	 * 
	 * @return The according observable {@link ObservablePropertiesBuilder}.
	 */
	public static ObservablePropertiesBuilder observe( PropertiesBuilder aProperties ) {
		return new ObservablePropertiesBuilderDecorator( aProperties );
	}

	/**
	 * Create an {@link ObservableResourcePropertiesBuilder} by decorating the
	 * provided {@link ResourcePropertiesBuilder} with observable functionality.
	 * 
	 * @param aProperties The {@link ResourcePropertiesBuilder} to be decorated
	 *        with {@link ObservableResourcePropertiesBuilder} functionality.
	 * 
	 * @return The according observable
	 *         {@link ObservableResourcePropertiesBuilder}.
	 */
	public static ObservableResourcePropertiesBuilder observe( ResourcePropertiesBuilder aProperties ) {
		return new ObservableResourcePropertiesBuilderDecorator( aProperties );
	}
}
