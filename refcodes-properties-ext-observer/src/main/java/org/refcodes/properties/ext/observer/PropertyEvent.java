// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import org.refcodes.observer.AbstractActionEvent;
import org.refcodes.observer.ActionEvent;
import org.refcodes.struct.Property;

/**
 * The {@link PropertyEvent} represents the base event type for the
 * {@link ObservableProperties}. A {@link PropertyEvent} represents a
 * {@link Property} with a key and a value (as of {@link Property#getKey()} and
 * {@link Property#getValue()}) which has been created, updated or deleted (as
 * of the {@link PropertyAction} enumeration). The {@link PropertyAction}
 * accessible via {@link PropertyEvent#getAction()} details the operation in
 * question.
 */
public class PropertyEvent extends AbstractActionEvent<PropertyAction, ObservableProperties> implements ActionEvent<PropertyAction, ObservableProperties>, Property {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _key;
	private final String _value;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link PropertyEvent} with the given key and value
	 * representing the affected {@link Property}.
	 * 
	 * @param aKey The key of the {@link Property} being involved in the event.
	 * @param aValue The value of the {@link Property} being involved in the
	 *        event.
	 * @param aAction The action which the {@link PropertyEvent} represents.
	 * @param aSource The source from which this event originated.
	 */
	public PropertyEvent( String aKey, String aValue, PropertyAction aAction, ObservableProperties aSource ) {
		super( aAction, aSource );
		_key = aKey;
		_value = aValue;
	}

	/**
	 * Constructs a {@link PropertyEvent} with the given key and value
	 * representing the affected {@link Property}.
	 * 
	 * @param aProperty The key and value pair of the {@link Property} being
	 *        involved in the event.
	 * @param aAction The action which the {@link PropertyEvent} represents.
	 * @param aSource The source from which this event originated.
	 */
	public PropertyEvent( Property aProperty, PropertyAction aAction, ObservableProperties aSource ) {
		super( aAction, aSource );
		_key = aProperty.getKey();
		_value = aProperty.getValue();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getKey() {
		return _key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getValue() {
		return _value;
	}
}
