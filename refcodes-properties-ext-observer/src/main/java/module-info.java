module org.refcodes.properties.ext.observer {
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.observer;
	requires transitive org.refcodes.runtime;
	requires transitive org.refcodes.struct;

	exports org.refcodes.properties.ext.observer;
}
