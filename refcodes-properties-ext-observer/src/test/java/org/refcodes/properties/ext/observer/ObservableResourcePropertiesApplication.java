// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import static org.refcodes.properties.PropertiesSugar.*;
import static org.refcodes.properties.ext.observer.ObservablePropertiesSugar.*;

import java.io.IOException;
import java.text.ParseException;

import org.refcodes.properties.Properties;
import org.refcodes.properties.ReloadMode;
import org.refcodes.properties.ext.observer.ObservableResouceProperties.ObservableResourcePropertiesBuilder;

public class ObservableResourcePropertiesApplication {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String[] args ) throws IOException, ParseException {
		final ObservableResourcePropertiesBuilder theObservableProperties = observe( seekFromTomlProperties( "application.config" ) );
		theObservableProperties.subscribeObserver( aEvent -> {
			System.out.println( "onEvent: Received an event of Action <" + aEvent.getAction() + "> with " + aEvent.getKey() + " := " + aEvent.getValue() );
			if ( "action".equalsIgnoreCase( aEvent.getKey() ) && "stop".equalsIgnoreCase( aEvent.getValue() ) ) {
				System.exit( 0 );
			}
		} );

		final Properties theProperties = schedule( theObservableProperties, ReloadMode.ORPHAN_REMOVAL );
		for ( String eKey : theProperties.keySet() ) {
			System.out.println( eKey + " := " + theProperties.get( eKey ) );
		}
	}
}
