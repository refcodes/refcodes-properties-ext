// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import static org.junit.jupiter.api.Assertions.*;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.properties.ext.observer.ObservableProperties.ObservablePropertiesBuilder;
import org.refcodes.properties.ext.observer.ObservableResouceProperties.ObservableResourcePropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public abstract class AbstractObservableResourcePropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _isUpdated;

	@Test
	public void testObservableFileProperties() throws IOException, ParseException {
		final File theFile = File.createTempFile( getClass().getName(), ".tmp" );
		theFile.deleteOnExit();
		final ObservableResourcePropertiesBuilder theResourceBuilder = toObservableResourcePropertiesBuilder();
		theResourceBuilder.put( "firstName", "Nolan" );
		theResourceBuilder.put( "lastName", "Tramiel" );
		theResourceBuilder.saveTo( theFile );
		final ObservableResourcePropertiesBuilder theListenerBuilder = toObservableResourcePropertiesBuilder();
		theListenerBuilder.loadFrom( theFile );
		assertEquals( "Nolan", theListenerBuilder.get( "firstName" ) );
		assertEquals( "Tramiel", theListenerBuilder.get( "lastName" ) );
		assertEquals( 2, theListenerBuilder.size() );
		_isUpdated = false;
		theListenerBuilder.subscribeObserver( aEvent -> {
			if ( aEvent.getAction() == PropertyAction.PROPERTY_UPDATED ) {
				if ( aEvent.getKey().equals( theListenerBuilder.toNormalizedPath( "lastName" ) ) && "Bushnell".equals( aEvent.getValue() ) ) {
					_isUpdated = true;
				}
			}
		} );
		theResourceBuilder.put( "lastName", "Bushnell" );
		theResourceBuilder.flush();
		theListenerBuilder.reload();
		assertEquals( true, _isUpdated );
	}

	@Test
	public void testObservableResourceProperties() throws IOException, ParseException {
		final ObservableResourcePropertiesBuilder theResourceBuilder = toObservableResourcePropertiesBuilder();
		final PropertiesBuilder theReference = new PropertiesBuilderImpl();
		final TestPropertiesObserver theObserver = new TestPropertiesObserver( theReference );
		theResourceBuilder.subscribeObserver( theObserver );
		theResourceBuilder.put( "firstName", "Nolan" );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.put( "firstName", "Jack" );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.put( "firstName", "Jack" );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.put( "lastName", "Tramiel" );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.put( "lastName", "Bushnell" );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.seekFrom( toResourceFileName() );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.put( "/commodore/user/firstName", "Konrad" );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.seekFrom( toResourceFileName() );
		assertSynced( theResourceBuilder, theReference );
		theResourceBuilder.clear();
		assertSynced( theResourceBuilder, theReference );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "--------------------------------------------------------------------------------" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Created calls := " + theObserver.createdCount );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Updated calls := " + theObserver.updatedCount );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Deleted calls := " + theObserver.deletedCount );
		}
		assertEquals( 13, theObserver.createdCount );
		assertEquals( 4, theObserver.updatedCount );
		assertEquals( 13, theObserver.deletedCount );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	protected abstract ObservableResourcePropertiesBuilder toObservableResourcePropertiesBuilder();

	protected abstract String toResourceFileName();

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void assertSynced( ObservablePropertiesBuilder aBuilder, PropertiesBuilder aReference ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "--------------------------------------------------------------------------------" );
		}
		assertEquals( aBuilder.size(), aReference.size() );
		assertEquals( aBuilder.toMap(), aReference.toMap() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class TestPropertiesObserver implements PropertiesObserver {
		private final PropertiesBuilder _builder;
		public int createdCount = 0;
		public int updatedCount = 0;
		public int deletedCount = 0;

		public TestPropertiesObserver( PropertiesBuilder aBuilder ) {
			_builder = aBuilder;
		}

		@Override
		public void onEvent( PropertyEvent aEvent ) {
			// if (IS_LOG_TESTS_ENABLED) System.out.println( aEvent.getAction() + ": key := " + aEvent.getKey() +
			// ", value := " + aEvent.getValue() );
			switch ( aEvent.getAction() ) {
			case PROPERTY_CREATED -> _builder.put( aEvent );
			case PROPERTY_DELETED -> _builder.delete( aEvent );
			case PROPERTY_UPDATED -> _builder.put( aEvent );
			default -> {
			}
			}
		}

		@Override
		public void onPropertyCreatedEvent( PropertyCreatedEvent aEvent ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( aEvent.getAction() + ": key := " + aEvent.getKey() + ", value := " + aEvent.getValue() );
			}
			createdCount++;
		}

		@Override
		public void onPropertyUpdatedEvent( PropertyUpdatedEvent aEvent ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( aEvent.getAction() + ": key := " + aEvent.getKey() + ", value := " + aEvent.getValue() + ", previous := " + aEvent.getPreviousValue() );
			}
			updatedCount++;
		}

		@Override
		public void onPropertyDeletedEvent( PropertyDeletedEvent aEvent ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( aEvent.getAction() + ": key := " + aEvent.getKey() + ", value := " + aEvent.getValue() );
			}
			deletedCount++;
		}
	}
}
