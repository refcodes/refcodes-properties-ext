// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.observer;

import static org.refcodes.properties.PropertiesSugar.*;
import static org.refcodes.properties.ext.observer.ObservablePropertiesSugar.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.ext.observer.ObservableProperties.ObservablePropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class ObservablePropertiesSugarTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("Just for educational purposes")
	@Test
	public void blogExample() {
		final PropertiesBuilder theProperties = toPropertiesBuilder();
		final ObservablePropertiesBuilder theObservable = observe( theProperties );
		theObservable.subscribeObserver( aEvent -> {
			if ( aEvent.getAction() == PropertyAction.PROPERTY_UPDATED ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( aEvent.getClass().getSimpleName() + " (" + aEvent.getAction() + ") --> " + aEvent.getKey() + " := " + aEvent.getValue() );
				}
			}
		} );
		theProperties.put( toProperty( "/user/firstName", "Nolan" ) );
		theProperties.put( toProperty( "/user/lastName", "Bushnell" ) );
		theProperties.put( toProperty( "/user/firstName", "Jack" ) );
		theProperties.put( toProperty( "/user/lastName", "Tramiel" ) );
		theProperties.remove( "/user/firstName" );
		theProperties.remove( "/user/lastName" );
	}
}
