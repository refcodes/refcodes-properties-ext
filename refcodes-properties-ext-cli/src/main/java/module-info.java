module org.refcodes.properties.ext.cli {
	requires org.refcodes.data;
	requires org.refcodes.runtime;
	requires transitive org.refcodes.schema;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.cli;
	requires transitive org.refcodes.struct;

	exports org.refcodes.properties.ext.cli;
}
