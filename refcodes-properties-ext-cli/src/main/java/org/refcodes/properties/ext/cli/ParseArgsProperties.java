// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.cli;

import java.util.List;

import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.cli.Operand;
import org.refcodes.cli.Option;
import org.refcodes.cli.ParseArgs;
import org.refcodes.properties.Properties;

/**
 * The {@link ParseArgsProperties} represent {@link Properties} generated from
 * command line arguments being parsed by a command line syntax as of the
 * definition for the {@link ParseArgs}.
 */
public interface ParseArgsProperties extends Properties, ParseArgs {

	/**
	 * Evaluates the command line arguments and sets the identified properties
	 * in this {@link Properties} instance. Previously identified properties are
	 * reset upon invocation of this method. When provided, then an
	 * {@link Operand}'s alias is used as property name, else as fall back, the
	 * operand's (when being of sub-type {@link Option}) long-option or
	 * short-option is used. Make sure, to provide an alias to make the
	 * {@link Properties} work and feel as intended! {@inheritDoc}
	 */
	@Override
	Operand<?>[] evalArgs( String[] aArgs ) throws ArgsSyntaxException;

	/**
	 * Evaluates the command line arguments and sets the identified properties
	 * in this {@link Properties} instance. Previously identified properties are
	 * reset upon invocation of this method. When provided, then an
	 * {@link Operand}'s alias is used as property name, else as fall back, the
	 * operand's (when being of sub-type {@link Option}) long-option or
	 * short-option is used. Make sure, to provide an alias to make the
	 * {@link Properties} work and feel as intended! {@inheritDoc}
	 */
	@Override
	Operand<?>[] evalArgs( List<String> aArgs ) throws ArgsSyntaxException;

	/**
	 * Same as {@link #evalArgs(String[])} with the difference that the
	 * identified properties are inserted below the provided path.
	 * 
	 * @param aToPath The path below which to insert the identified properties.
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	Operand<?>[] evalArgs( String aToPath, List<String> aArgs ) throws ArgsSyntaxException;

	/**
	 * Same as {@link #evalArgs(String[])} with the difference that the
	 * identified properties are inserted below the provided path.
	 * 
	 * @param aToPath The path below which to insert the identified properties.
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return The list of evaluated command line arguments being instances of
	 *         the {@link Operand} interfaces or its sub-types.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	Operand<?>[] evalArgs( String aToPath, String[] aArgs ) throws ArgsSyntaxException;

	/**
	 * Same as {@link #evalArgs(String[])} with the difference that this class's
	 * instance being invoked is returned as of the builder pattern.
	 *
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return This invoked instance as of the builder pattern to chain method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	@Override
	default ParseArgsProperties withEvalArgs( String[] aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs );
		return this;
	}

	/**
	 * Same as {@link #evalArgs(List)} with the difference that this class's
	 * instance being invoked is returned as of the builder pattern.
	 *
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return This invoked instance as of the builder pattern to chain method
	 *         calls.
	 *
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	@Override
	default ParseArgsProperties withEvalArgs( List<String> aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs.toArray( new String[aArgs.size()] ) );
		return this;
	}

	/**
	 * Same as {@link #evalArgs(String[])} with the difference that the
	 * identified properties are inserted below the provided path and that this
	 * class's instance being invoked is returned as of the builder pattern.
	 * 
	 * @param aToPath The path below which to insert the identified properties.
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return This invoked instance as of the builder pattern to chain method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default ParseArgsProperties withEvalArgs( String aToPath, List<String> aArgs ) throws ArgsSyntaxException {
		evalArgs( aArgs.toArray( new String[aArgs.size()] ) );
		return this;
	}

	/**
	 * Same as {@link #evalArgs(String[])} with the difference that the
	 * identified properties are inserted below the provided path and that this
	 * class's instance being invoked is returned as of the builder pattern.
	 * 
	 * @param aToPath The path below which to insert the identified properties.
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return This invoked instance as of the builder pattern to chain method
	 *         calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	default ParseArgsProperties withEvalArgs( String aToPath, String[] aArgs ) throws ArgsSyntaxException {
		evalArgs( aToPath, aArgs );
		return this;
	}
}
