// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.cli;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.cli.CliSugar.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.refcodes.cli.AndCondition;
import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.cli.Condition;
import org.refcodes.cli.Flag;
import org.refcodes.cli.Operand;
import org.refcodes.cli.Option;
import org.refcodes.cli.OrCondition;
import org.refcodes.cli.StringOperand;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.XorCondition;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;

public class ArgsParserPropertiesTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	private static final String[][] ARGS = new String[][] { 
		{ "--name", "FILE", "--boolean", "--active" }, 
		{ "--name", "FILE", "--boolean", "--alias", "ALIAS" }, 
		{ "--name", "FILE", "--boolean", "--alias", "ALIAS", "--active" }, 
		{ "--name", "FILE", "--boolean", "--alias", "ALIAS", "NULL" }, 
		{ "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS" }, 
		{ "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS", "NIL" }, 
		{ "NULL", "--name", "NAME0", "--name", "NAME1", "--boolean", "--alias", "ALIAS", "NIL" }
	};
	
	private static final String[][] PROPERTIES = new String[][] { 
		{ "name=FILE", "boolean=true", "active=true" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS", "active=true" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS", "null=NULL" }, 
		{ "null=NULL", "name=FILE", "boolean=true", "alias=ALIAS" }, 
		{ "name=FILE", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" },
		{ "name/0=NAME0", "name/1=NAME1", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" }
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testParseArgs() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from", "The source file to be processed, please provide a relative path as absolute paths are not permitted because absolute pahts may prevent path jail braking." );
		final Option<String> theToFile = new StringOption( 't', "to", "to", "The destination file to be processed" );
		final Flag theAddFile = new Flag( 'a', "add", "add", "Add the specified file" );
		final Flag theCreateFile = new Flag( 'c', "create", "create", "Creates the specified file" );
		final Condition theOrArgsSyntax = new OrCondition( theAddFile, theCreateFile );
		final Flag theDeleteFile = new Flag( 'd', "delete", "delete", "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed, it can be a complex arithmetic expression or a plain placeholder representing an environment variable." );
		final Condition theXor = new XorCondition( theOrArgsSyntax, theDeleteFile );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f", "fromFile", "-t", "toFile", "-a", "anOperand" };
		final ParseArgsProperties theArgsProperties = new ArgsParserProperties( theAnd );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theAnd.toSynopsis() );
		}
		theArgsProperties.evalArgs( args );
		for ( String eKey : theArgsProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theArgsProperties.get( eKey ) );
			}
		}
		final String theFrom = theArgsProperties.get( "from" );
		final String theTo = theArgsProperties.get( "to" );
		final boolean theAdd = theArgsProperties.getBoolean( "add" );
		final boolean theCreate = theArgsProperties.getBoolean( "create" );
		final boolean theDelete = theArgsProperties.getBoolean( "delete" );
		assertEquals( "fromFile", theFrom );
		assertEquals( "toFile", theTo );
		assertTrue( theAdd );
		assertFalse( theCreate );
		assertFalse( theDelete );
	}

	@Test
	public void testArgsPropertiesToPath() throws ArgsSyntaxException {
		final Option<String> theFromFile = new StringOption( 'f', "from", "from", "The source file to be processed, please provide a relative path as absolute paths are not permitted because absolute pahts may prevent path jail braking." );
		final Option<String> theToFile = new StringOption( 't', "to", "to", "The destination file to be processed" );
		final Flag theAddFile = new Flag( 'a', "add", "add", "Add the specified file" );
		final Flag theCreateFile = new Flag( 'c', "create", "create", "Creates the specified file" );
		final Condition theOrArgsSyntax = new OrCondition( theAddFile, theCreateFile );
		final Flag theDeleteFile = new Flag( 'd', "delete", "delete", "Delete the specified file" );
		final Operand<String> theOperand = new StringOperand( "operand", "The operand to be processed, it can be a complex artihmetic expression or a plain placeholder representing an environment variable." );
		final Condition theXor = new XorCondition( theOrArgsSyntax, theDeleteFile );
		final Condition theAnd = new AndCondition( theXor, theFromFile, theToFile, theOperand );
		final String[] args = new String[] { "-f", "fromFile", "-t", "toFile", "-a", "anOperand" };
		final ParseArgsProperties theArgsProperties = new ArgsParserProperties( theAnd );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( theAnd.toSynopsis() );
		}
		theArgsProperties.evalArgs( "arguments", args );
		for ( String eKey : theArgsProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theArgsProperties.get( eKey ) );
			}
		}
		final String theFrom = theArgsProperties.get( "/arguments/from" );
		final String theTo = theArgsProperties.get( "/arguments/to" );
		final boolean theAdd = theArgsProperties.getBoolean( "/arguments/add" );
		final boolean theCreate = theArgsProperties.getBoolean( "/arguments/create" );
		final boolean theDelete = theArgsProperties.getBoolean( "/arguments/delete" );
		assertEquals( "fromFile", theFrom );
		assertEquals( "toFile", theTo );
		assertTrue( theAdd );
		assertFalse( theCreate );
		assertFalse( theDelete );
	}

	@Test
	public void testArgsProperties() throws ArgsSyntaxException {
		for ( int i = 0; i < ARGS.length; i++ ) {
			testArgs( i );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
	}

	@Test
	public void testEdgeCase() throws ArgsSyntaxException {
		testArgs( 6 );
	}

	@Test
	public void testArrayProperties() throws IOException, ArgsSyntaxException {
		final Option<String[]> theStringArray = asArray( stringOption( 't', "text", "text", "Some text" ), 3, 3 );
		final Option<Integer[]> theIntArray = asArray( intOption( 'i', "int", "int", "Some integer" ), 1, 4 );
		final Condition theRoot = and( theStringArray, theIntArray );
		final ParseArgsProperties theProperties = new ArgsParserProperties( theRoot );
		theProperties.setDescription( "Some runtime properties test." );
		theProperties.setTitle( "TEST" );
		final String[] theArgs = new String[] { "-t", "text1", "-i", "1", "-t", "text2", "-i", "2", "-t", "text3", "-i", "3" };
		theProperties.evalArgs( theArgs );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			theProperties.printHelp();
			final Set<String> theKeySet = theProperties.keySet();
			final List<String> theKeys = new ArrayList<>( theKeySet );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " = " + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( theProperties.get( "/int/0" ), "1" );
		assertEquals( theProperties.get( "/int/1" ), "2" );
		assertEquals( theProperties.get( "/int/2" ), "3" );
		assertEquals( theProperties.get( "/text/0" ), "text1" );
		assertEquals( theProperties.get( "/text/1" ), "text2" );
		assertEquals( theProperties.get( "/text/2" ), "text3" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected void testArgs( int i ) throws ArgsSyntaxException {
		final ParseArgsProperties theProperties = new ArgsParserProperties().withEvalArgs( ARGS[i] );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Line <" + i + ">:" );
		}
		for ( String eKey : theProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " := " + theProperties.get( eKey ) );
			}
		}
		final String[] theExcepcted = PROPERTIES[i];
		for ( String aTheExcepcted : theExcepcted ) {
			final Property eProperty = new PropertyImpl( aTheExcepcted );
			assertEquals( eProperty.getValue(), theProperties.get( eProperty.getKey() ), "Line <" + i + "> (" + eProperty.toString() + ")" );
		}
	}
}
