# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact extends the [`refcodes-properties`](https://www.metacodes.pro/refcodes/refcodes-properties) toolkit with functionality from the [`refcodes-cli`](https://www.metacodes.pro/refcodes/refcodes-cli) toolkit, integrating command line arguments into the [`ArgsParserProperties`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli/latest/org.refcodes.properties.ext.cli/org/refcodes/properties/ext/cli/ArgsParserProperties.html) sub-type of the [`Properties`](https://static.javadoc.io/org.refcodes/refcodes-properties/latest/org.refcodes.properties/org/refcodes/properties/Properties.html) type.***

## Getting started ##

> Please refer to the [refcodes-properties: Chaos-based encryption as Java cryptographic extension (and without)](https://www.metacodes.pro/refcodes/refcodes-properties) documentation for an up-to-date and detailed description on the usage of this artifact. 

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-properties-ext-cli</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-properties-alt/src/master/refcodes-properties-ext-cli). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-cli).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.