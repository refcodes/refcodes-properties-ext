// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.application;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.properties.ext.application.ApplicationPropertiesSugar.*;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.TomlPropertiesBuilder;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.runtime.SystemContext;
import org.refcodes.struct.Property;
import org.refcodes.struct.PropertyImpl;

public class ApplicationPropertiesSugarTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String[][] ARGS = new String[][] { { "--name", "FILE", "--boolean", "--active" }, { "--name", "FILE", "--boolean", "--alias", "ALIAS" }, { "--name", "FILE", "--boolean", "--alias", "ALIAS", "--active" }, { "--name", "FILE", "--boolean", "--alias", "ALIAS", "NULL" }, { "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS" }, { "NULL", "--name", "FILE", "--boolean", "--alias", "ALIAS", "NIL" }, { "NULL", "--name", "NAME0", "--name", "NAME1", "--boolean", "--alias", "ALIAS", "NIL" } };
	private static final String[][] PROPERTIES = new String[][] { { "name=FILE", "boolean=true", "active=true" }, { "name=FILE", "boolean=true", "alias=ALIAS" }, { "name=FILE", "boolean=true", "alias=ALIAS", "active=true" }, { "name=FILE", "boolean=true", "alias=ALIAS", "null=NULL" }, { "null=NULL", "name=FILE", "boolean=true", "alias=ALIAS" }, { "name=FILE", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" }, { "name/0=NAME0", "name/1=NAME1", "boolean=true", "alias=ALIAS", "null/0=NULL", "null/1=NIL" } };

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testApplicationProperties() throws IOException, ParseException {
		System.setProperty( "commodore.user.firstName", "Bla" );
		final ApplicationProperties theProperties = withResourceClass( "application.config" );
		theProperties.setName( "ApplicationProperties" );
		// theProperties.setConsoleWidth( 120 );
		theProperties.printBanner();
		theProperties.printSynopsis();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final Set<String> theKeySet = theProperties.keySet();
			final List<String> theKeys = new ArrayList<>( theKeySet );
			Collections.sort( theKeys );
			for ( String eKey : theKeys ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( eKey + " = " + theProperties.get( eKey ) );
				}
			}
		}
		assertEquals( "Player", theProperties.get( "/user" ) );
		assertEquals( "Bla", theProperties.get( "/user/firstName" ) );
		assertEquals( "Tramiel", theProperties.get( "/user/lastName" ) );
		assertEquals( "commodore", theProperties.get( "/runtime/profiles" ) );
		assertEquals( "admin", theProperties.get( "/database/user" ) );
		assertEquals( "secret", theProperties.get( "/database/password" ) );
		assertEquals( "jdbc://my/database/url:5161", theProperties.get( "/database/url" ) );
		assertEquals( "240", theProperties.get( "/console/history" ) );
		assertEquals( "25", theProperties.get( "/console/height" ) );
		assertEquals( "true", theProperties.get( "/console/ansi" ) );
		assertEquals( "Player", theProperties.get( "user" ) );
		assertEquals( "Bla", theProperties.get( "user/firstName" ) );
		assertEquals( "Tramiel", theProperties.get( "user/lastName" ) );
		assertEquals( "Tramiel", theProperties.get( "user/lastName" ) );
		assertEquals( "commodore", theProperties.get( "runtime/profiles" ) );
		assertEquals( "admin", theProperties.get( "database/user" ) );
		assertEquals( "secret", theProperties.get( "database/password" ) );
		assertEquals( "jdbc://my/database/url:5161", theProperties.get( "database/url" ) );
		assertEquals( "240", theProperties.get( "console/history" ) );
		assertEquals( "25", theProperties.get( "console/height" ) );
		assertEquals( "true", theProperties.get( "console/ansi" ) );
	}

	@Test
	public void testArgsProperties() throws ArgsSyntaxException {
		for ( int i = 0; i < ARGS.length; i++ ) {
			testArgs( i );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "--------------------------------------------------------------------------------" );
			}
		}
	}

	@Test
	public void testObfuscationProperties() {
		final ResourcePropertiesBuilder theProperties = new TomlPropertiesBuilder();
		theProperties.put( "secret", "encrypt:secret" );
		final ApplicationProperties theApplicationProperties = withProperties( theProperties ).withObfuscationMode( SystemContext.HOST_USER_APPLICATION );
		final String theDecrypted = theApplicationProperties.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Secret = " + theDecrypted );
		}
		final String theEncrypted = theProperties.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypted secret = " + theEncrypted );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Decrypted secret = " + theDecrypted );
		}
		assertNotEquals( theEncrypted, theDecrypted );
		assertEquals( "secret", theDecrypted );
	}

	@Test
	public void testPropertiesPrecedence() {
		final ResourcePropertiesBuilder theProperties = new TomlPropertiesBuilder();
		theProperties.put( "env", "test" );
		final ApplicationProperties theApplicationProperties = withProperties( theProperties );
		String theEnv = theApplicationProperties.get( "env" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Environment = " + theEnv );
		}
		assertEquals( "test", theEnv );
		System.setProperty( "env", "prod" );
		theEnv = theApplicationProperties.get( "env" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Environment = " + theEnv );
		}
		assertEquals( "prod", theEnv );
	}

	// @Disabled
	@Test
	public void testEdgeCase() throws ArgsSyntaxException {
		testArgs( 5 );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "--------------------------------------------------------------------------------" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected void testArgs( int i ) throws ArgsSyntaxException {
		final ApplicationProperties theProperties = withParseArgs( ARGS[i] );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Line <" + i + ">:" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "--------------------------------------------------------------------------------" );
		}
		for ( String eKey : theProperties.keySet() ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eKey + " = " + theProperties.get( eKey ) );
			}
		}
		final String[] theExcepcted = PROPERTIES[i];
		for ( String aTheExcepcted : theExcepcted ) {
			final Property eProperty = new PropertyImpl( aTheExcepcted );
			assertEquals( eProperty.getValue(), theProperties.get( eProperty.getKey() ), "Line <" + i + "> (" + eProperty.toString() + ")" );
		}
	}
}
