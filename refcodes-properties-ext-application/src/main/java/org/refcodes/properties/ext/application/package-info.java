// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact extends the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-properties"><code>refcodes-properties</code></a>
 * artifact with predefined <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-properties/3.0.0/org.refcodes.properties/org/refcodes/properties/Properties.html"><code>Properties</code></a>
 * compositions (as of the <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-application/latest/org.refcodes.properties.ext.application/org/refcodes/properties/ext/runtime/ApplicationProperties.html"><code>ApplicationProperties</code></a>
 * and the <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-application/latest/org.refcodes.properties.ext.application/org/refcodes/properties/ext/runtime/ApplicationProperties.html"><code>ApplicationProperties</code></a>
 * types) for common everyday application setups (also see the blog post
 * <a href=
 * "http://www.refcodes.org/blog/all-in-one_java_configuration_properties_at_hand">All-in-one
 * Java configuration properties at hand</a>).
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-properties"><strong>refcodes-properties:
 * Managing your application's configuration</strong></a> documentation for an
 * up-to-date and detailed description on the usage of this artifact.
 * </p>
 */
package org.refcodes.properties.ext.application;
