// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.application;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;

import org.refcodes.cli.ArgsSyntaxException;
import org.refcodes.cli.Condition;
import org.refcodes.cli.Option;
import org.refcodes.properties.Properties;
import org.refcodes.properties.PropertiesSugar;
import org.refcodes.runtime.ConfigLocator;
import org.refcodes.runtime.SystemContext;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the construction of
 * {@link ApplicationProperties} (and the like).
 */
public class ApplicationPropertiesSugar extends PropertiesSugar {

	// /////////////////////////////////////////////////////////////////////////
	// JAVA PROPERTIES:
	// /////////////////////////////////////////////////////////////////////////

	private ApplicationPropertiesSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Constructs a
	 * {@link ApplicationProperties} instance with no {@link Condition} for
	 * parsing command line arguments. As no syntax notation is required by the
	 * constructor (no root {@link Condition}), no syntax validation is done.
	 * Therefore the properties are heuristically determined from the provided
	 * command line arguments when invoking {@link #withArgs(String[])}.
	 * 
	 * @return The accordingly constructed {@link ApplicationProperties}
	 *         instance.
	 */
	public static ApplicationProperties fromApplicationProperties() {
		return new ApplicationProperties();
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Constructs a
	 * {@link ApplicationProperties} instance with no {@link Condition} for
	 * parsing command line arguments: As no syntax notation is required by the
	 * constructor (no root {@link Condition}), no syntax validation is done.
	 * Therefore the properties are heuristically determined from the provided
	 * command line arguments.
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return The accordingly constructed {@link ApplicationProperties}
	 *         instance.
	 */
	public static ApplicationProperties withArgs( String[] aArgs ) {
		return new ApplicationProperties( aArgs );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Constructs a
	 * {@link ApplicationProperties} instance with the given {@link Condition}
	 * representing the syntax to be used to parse command line arguments.
	 * Provide command line arguments via
	 * {@link ApplicationProperties#evalArgs(String[])} (and the like methods).
	 * 
	 * @param aRootArgsSyntax The root condition being the node from which
	 *        parsing the command line arguments starts. Parse the command line
	 *        arguments via {@link ApplicationProperties#evalArgs(String[])}.
	 * 
	 * @return The accordingly constructed {@link ApplicationProperties}
	 *         instance.
	 */
	public static ApplicationProperties withRootArgsSyntax( Condition aRootArgsSyntax ) {
		return new ApplicationProperties( aRootArgsSyntax );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Constructs a
	 * {@link ApplicationProperties} instance with the given {@link Option}
	 * representing the syntax to be used to parse command line arguments.
	 * Provide command line arguments via
	 * {@link ApplicationProperties#evalArgs(String[])} (and the like methods).
	 * 
	 * @param aRootOption The root option being the node from which parsing the
	 *        command line arguments starts. Parse the command line arguments
	 *        via {@link ApplicationProperties#evalArgs(String[])}.
	 * 
	 * @return The accordingly constructed {@link ApplicationProperties}
	 *         instance.
	 */
	public static ApplicationProperties withRootOption( Option<?> aRootOption ) {
		return new ApplicationProperties( aRootOption );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Constructs the
	 * {@link ApplicationProperties} with the given obfuscation mode
	 * 
	 * @param aMode The {@link SystemContext} specifies which level of
	 *        obfuscation is to be used when encountering upon obfuscated
	 *        properties: E.g. obfuscation may be bound to the host, the
	 *        "secret" used for obfuscation being the same for all applications
	 *        on the same host or obfuscation may be bound to the application,
	 *        being different for different applications on the same host.
	 * 
	 * @return The accordingly constructed {@link ApplicationProperties}
	 *         instance.
	 */
	public static ApplicationProperties withObfuscationMode( SystemContext aMode ) {
		return new ApplicationProperties( aMode );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Constructs a
	 * {@link ApplicationProperties} instance with a custom secret for
	 * obfuscation.
	 * 
	 * @param aSecret The secret to be used when encountering upon obfuscated
	 *        properties.
	 * 
	 * @return The accordingly constructed {@link ApplicationProperties}
	 *         instance.
	 */
	public static ApplicationProperties withSecret( String aSecret ) {
		return new ApplicationProperties( aSecret );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Loads a properties file from the
	 * file directly or (if not found) from first folder containing such a file
	 * as of the specification for the method
	 * {@link ConfigLocator#getFolders()}.
	 * 
	 * @param aFile The file of the properties to load.
	 * 
	 * @return The {@link ApplicationProperties} instance as of the builder
	 *         pattern for chained method calls.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public static ApplicationProperties withFile( File aFile ) throws IOException, ParseException {
		return fromApplicationProperties().withFile( aFile );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Reads the properties from the
	 * given {@link InputStream}.
	 *
	 * @param aInputStream The {@link InputStream} from which to read the
	 *        properties.
	 * 
	 * @return The {@link ApplicationProperties} instance as of the builder
	 *         pattern for chained method calls.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public static ApplicationProperties withInputStream( InputStream aInputStream ) throws IOException, ParseException {
		return fromApplicationProperties().withInputStream( aInputStream );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Loads a properties file from the
	 * file directly or (if not found) from first folder containing such a file
	 * as of the specification for the method
	 * {@link ConfigLocator#getFolders()}.
	 * 
	 * @param aFilePath The file of the properties file to load.
	 * 
	 * @return The {@link ApplicationProperties} instance as of the builder
	 *         pattern for chained method calls.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public static ApplicationProperties withResourceClass( String aFilePath ) throws IOException, ParseException {
		return fromApplicationProperties().withResourceClass( aFilePath );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Loads the properties from the
	 * given {@link URL}.
	 *
	 * @param aUrl The {@link URL} from which to read the properties.
	 * 
	 * @return The {@link ApplicationProperties} instance as of the builder
	 *         pattern for chained method calls.
	 * 
	 * @throws IOException thrown in case accessing or processing the properties
	 *         file failed.
	 * @throws ParseException Signals that an error has been reached
	 *         unexpectedly while parsing the data to be loaded.
	 */
	public static ApplicationProperties withUrl( URL aUrl ) throws IOException, ParseException {
		return fromApplicationProperties().withUrl( aUrl );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. Evaluates the provided command
	 * line arguments and adds them with their according properties
	 * representation
	 * 
	 * @param aArgs The command line arguments to be evaluated.
	 * 
	 * @return The {@link ApplicationProperties} instance of the builder pattern
	 *         to chain further method calls.
	 * 
	 * @throws ArgsSyntaxException thrown in case of a command line arguments
	 *         mismatch regarding provided and expected args.
	 */
	public static ApplicationProperties withParseArgs( String[] aArgs ) throws ArgsSyntaxException {
		return fromApplicationProperties().withEvalArgs( aArgs );
	}

	/**
	 * Factory method to conveniently create a preconfigured
	 * {@link ApplicationProperties} instance. A hook for you to provide
	 * {@link Properties} programmatically. The later you add
	 * {@link Properties}, the lower their precedence.
	 * 
	 * @param aProperties The {@link Properties} to be added.
	 * 
	 * @return The {@link ApplicationProperties} instance of the builder pattern
	 *         to chain further method calls.
	 */
	public static ApplicationProperties withProperties( Properties aProperties ) {
		return fromApplicationProperties().withProperties( aProperties );
	}
}
