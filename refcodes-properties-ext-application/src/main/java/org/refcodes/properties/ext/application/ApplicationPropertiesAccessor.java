// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.application;

/**
 * Provides an accessor for a {@link ApplicationProperties} property.
 */
public interface ApplicationPropertiesAccessor {

	/**
	 * Retrieves the runtime properties from the {@link ApplicationProperties}
	 * property.
	 * 
	 * @return The runtime properties stored by the
	 *         {@link ApplicationProperties} property.
	 */
	ApplicationProperties getApplicationProperties();

	/**
	 * Provides a mutator for a {@link ApplicationProperties} property.
	 */
	public interface ApplicationPropertiesMutator {

		/**
		 * Sets the runtime properties for the {@link ApplicationProperties}
		 * property.
		 * 
		 * @param aApplicationProperties The runtime properties to be stored by
		 *        the {@link ApplicationProperties} property.
		 */
		void setApplicationProperties( ApplicationProperties aApplicationProperties );
	}

	/**
	 * Provides a builder method for a {@link ApplicationProperties} property
	 * returning the builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface ApplicationPropertiesBuilder<B extends ApplicationPropertiesBuilder<B>> {

		/**
		 * Sets the runtime properties for the {@link ApplicationProperties}
		 * property.
		 * 
		 * @param aApplicationProperties The runtime properties to be stored by
		 *        the {@link ApplicationProperties} property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withApplicationProperties( ApplicationProperties aApplicationProperties );
	}

	/**
	 * Provides a {@link ApplicationProperties} property.
	 */
	public interface ApplicationPropertiesProperty extends ApplicationPropertiesAccessor, ApplicationPropertiesMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ApplicationProperties} (setter) as of
		 * {@link #setApplicationProperties(ApplicationProperties)} and returns
		 * the very same value (getter).
		 * 
		 * @param aApplicationProperties The {@link ApplicationProperties} to
		 *        set (via
		 *        {@link #setApplicationProperties(ApplicationProperties)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ApplicationProperties letApplicationProperties( ApplicationProperties aApplicationProperties ) {
			setApplicationProperties( aApplicationProperties );
			return aApplicationProperties;
		}
	}
}
