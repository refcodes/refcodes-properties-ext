module org.refcodes.properties.ext.application {
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.properties.ext.obfuscation;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.security;
	requires transitive org.refcodes.security.alt.chaos;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.properties.ext.cli;
	requires transitive org.refcodes.cli;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.runtime;
	requires transitive org.refcodes.struct;
	requires transitive org.refcodes.textual;

	exports org.refcodes.properties.ext.application;
}
