# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact extends the [`refcodes-properties`](https://www.metacodes.pro/refcodes/refcodes-properties) artifact with predefined [`Properties`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties/3.0.0/org.refcodes.properties/org/refcodes/properties/Properties.html) compositions (as of the [`ApplicationProperties`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/latest/org.refcodes.properties.ext.application/org/refcodes/properties/ext/runtime/ApplicationProperties.html) and the [`ApplicationProperties`](https://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime/latest/org.refcodes.properties.ext.application/org/refcodes/properties/ext/runtime/ApplicationProperties.html) types) for common everyday application setups (also see the blog post [All-in-one Java configuration properties at hand](http://www.refcodes.org/blog/all-in-one_java_configuration_properties_at_hand)).***

## Getting started ##

> Please refer to the [refcodes-properties: Managing your application's configuration](https://www.metacodes.pro/refcodes/refcodes-properties) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-properties-ext-runtime</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-properties-ext/src/master/refcodes-properties-ext-runtime). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-properties-ext-runtime).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.