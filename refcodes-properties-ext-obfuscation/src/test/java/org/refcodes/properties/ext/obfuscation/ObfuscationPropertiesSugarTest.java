// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import static org.refcodes.properties.PropertiesSugar.*;
import static org.refcodes.properties.ext.obfuscation.ObfuscationPropertiesSugar.*;
import java.io.IOException;
import java.text.ParseException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.ext.obfuscation.ObfuscationResourceProperties.ObfuscationResourcePropertiesBuilder;
import org.refcodes.runtime.SystemContext;

/**
 *
 */
public class ObfuscationPropertiesSugarTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	@Disabled
	public void testObfuscationResourceProperties1() throws IOException, ParseException {
		final ResourcePropertiesBuilder properties = seekFromJavaProperties( "application.properties" );
		final ObfuscationResourcePropertiesBuilder obfuscateProperties = obfuscate( properties );
		obfuscateProperties.put( "secret", "encrypt:Hello world!" );
		obfuscateProperties.flush();
	}

	@Test
	@Disabled
	public void testObfuscationResourceProperties2() throws IOException, ParseException {
		final ResourcePropertiesBuilder properties = seekFromJavaProperties( "application.properties" );
		final ObfuscationResourcePropertiesBuilder obfuscateProperties = obfuscate( properties, SystemContext.HOST_USER );
		obfuscateProperties.put( "secret", "encrypt:Hello world!" );
		obfuscateProperties.flush();
	}
}
