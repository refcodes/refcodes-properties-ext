// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.TomlPropertiesBuilder;
import org.refcodes.properties.ext.obfuscation.ObfuscationResourceProperties.ObfuscationResourcePropertiesBuilder;
import org.refcodes.runtime.SystemProperty;

public class ObfuscationPropertiesBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testObfuscationPropertiesBuilder1() throws IOException {
		final ResourcePropertiesBuilder theProperties = new TomlPropertiesBuilder();
		final String theSecret = "1234567890";
		theProperties.put( "secret", "encrypt:" + theSecret );
		final String theEncrypt = theProperties.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypt = " + theEncrypt );
		}
		final ObfuscationResourcePropertiesBuilder theObfuscated = new ObfuscationResourcePropertiesBuilderDecorator( theProperties );
		final String theEncrypted = theProperties.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypted = " + theEncrypted );
		}
		assertNotEquals( theEncrypt, theEncrypted );
		final String theValue = theObfuscated.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Decrypted = " + theValue );
		}
		assertEquals( theSecret, theValue );
	}

	@Test
	public void testObfuscationPropertiesBuilder2() throws IOException {
		final ResourcePropertiesBuilder theProperties = new TomlPropertiesBuilder();
		final String theSecret = "1234567890";
		theProperties.put( "secret", "encrypt:" + theSecret );
		final String theEncrypt = theProperties.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypt = " + theEncrypt );
		}
		final ObfuscationResourcePropertiesBuilder theObfuscated = new ObfuscationResourcePropertiesBuilderDecorator( theProperties, "mySecret" );
		final String theEncrypted = theProperties.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypted = " + theEncrypted );
		}
		assertNotEquals( theEncrypt, theEncrypted );
		final String theValue = theObfuscated.get( "secret" );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Decrypted = " + theValue );
		}
		assertEquals( theSecret, theValue );
	}
}
