// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import java.util.HashSet;
import java.util.Set;

import org.refcodes.data.Prefix;
import org.refcodes.properties.AbstractPropertiesBuilderDecorator;
import org.refcodes.properties.Properties;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.ext.obfuscation.ObfuscationProperties.ObfuscationPropertiesBuilder;

/**
 * Base implementation for a {@link ObfuscationPropertiesBuilder} flavor.
 */
public abstract class AbstractObfuscationPropertiesBuilderDecorator extends AbstractPropertiesBuilderDecorator<PropertiesBuilder> implements ObfuscationPropertiesBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _decryptPrefix = Prefix.DECRYPT.getPrefix();
	protected String _encryptPrefix = Prefix.ENCRYPT.getPrefix();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public AbstractObfuscationPropertiesBuilderDecorator( PropertiesBuilder aProperties ) {
		super( aProperties );

	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEncryptPrefix() {
		return _encryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEncryptPrefix( String aEncryptPrefix ) {
		_encryptPrefix = aEncryptPrefix;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDecryptPrefix() {
		return _decryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDecryptPrefix( String aDecryptPrefix ) {
		_decryptPrefix = aDecryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		String theValue = super.get( aKey );
		if ( theValue != null ) {
			if ( theValue.startsWith( _encryptPrefix ) ) {
				theValue = theValue.substring( _encryptPrefix.length() );
			}
			else if ( theValue.startsWith( _decryptPrefix ) ) {
				theValue = theValue.substring( _decryptPrefix.length() );
				theValue = toDecrypted( theValue );
			}
		}
		return theValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String put( String aKey, String aValue ) {
		if ( aValue != null ) {
			if ( aValue.startsWith( _encryptPrefix ) ) {
				aValue = aValue.substring( _encryptPrefix.length() );
				aValue = toEncrypted( aValue );
				aValue = _decryptPrefix + aValue;
			}
		}
		return super.put( aKey, aValue );
	}

	// /////////////////////////////////////////////////////////////////////////
	// SUB-TYPES:
	// /////////////////////////////////////////////////////////////////////////

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveFrom( String aFromPath ) {
	//		return getProperties().retrieveFrom( aFromPath );
	//	}
	//
	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public PropertiesBuilder retrieveTo( String aToPath ) {
	//		return getProperties().retrieveTo( aToPath );
	//	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Encrypts the given value. No prefixing is to be done by this method!
	 * 
	 * @param aValue The value to be encrypted.
	 * 
	 * @return The encrypted value.
	 */
	abstract String toEncrypted( String aValue );

	/**
	 * Decrypts the given value. No prefixing is to be done by this method!
	 * 
	 * @param aValue The value to be decrypted.
	 * 
	 * @return The decrypted value.
	 */
	abstract String toDecrypted( String aValue );

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Encrypts all values prefixed accordingly as returned by
	 * {@link #getEncryptPrefix()}. The properties do not contain any
	 * "to-be-encrypted" values any more, them values then are encrypted. The
	 * changes are only done "in-memory" and are not applied to the resource.
	 * You must save the properties yourself to persist the changes.
	 * 
	 * @return True when properties have been changed (encrypted) by this
	 *         method.
	 */
	protected boolean encryptAll() {
		boolean isDirty = false;
		String eValue;
		for ( String eKey : keySet() ) {
			eValue = super.get( eKey );
			if ( eValue != null && eValue.startsWith( _encryptPrefix ) ) {
				eValue = eValue.substring( _encryptPrefix.length() );
				eValue = toEncrypted( eValue );
				super.put( eKey, _decryptPrefix + eValue );
				isDirty = true;
			}
		}
		return isDirty;
	}

	/**
	 * Returns a {@link Set} of all the keys which's values are to be encrypted,
	 * e.g. which's values are prefixed with the prefix as returned by
	 * {@link #getEncryptPrefix()}.
	 * 
	 * @return The key {@link Set} of all to be encrypted values.
	 */
	protected Set<String> encryptSet() {
		final Set<String> theResult = new HashSet<>();
		String eValue;
		for ( String eKey : keySet() ) {
			eValue = get( eKey );
			if ( eValue != null && eValue.startsWith( _encryptPrefix ) ) {
				theResult.add( eKey );
			}
		}
		return theResult;
	}
}
