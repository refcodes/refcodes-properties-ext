// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import java.io.IOException;

import org.refcodes.properties.Properties;
import org.refcodes.properties.Properties.PropertiesBuilder;
import org.refcodes.properties.PropertiesSugar;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.ext.obfuscation.ObfuscationProperties.ObfuscationPropertiesBuilder;
import org.refcodes.properties.ext.obfuscation.ObfuscationResourceProperties.ObfuscationResourcePropertiesBuilder;
import org.refcodes.runtime.SystemContext;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the construction of
 * {@link ObfuscationProperties} (and the like).
 */
public class ObfuscationPropertiesSugar extends PropertiesSugar {

	// /////////////////////////////////////////////////////////////////////////
	// JAVA PROPERTIES:
	// /////////////////////////////////////////////////////////////////////////

	private ObfuscationPropertiesSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * Create an {@link ObfuscationProperties} by decorating the provided
	 * {@link Properties} with obfuscation functionality.
	 * 
	 * @param aProperties The {@link Properties} to be decorated with
	 *        {@link ObfuscationProperties} functionality.
	 * 
	 * @return The according obfuscating {@link ObfuscationProperties}.
	 */
	public static ObfuscationProperties obfuscate( Properties aProperties ) {
		return new ObfuscationPropertiesDecorator( aProperties );
	}

	/**
	 * Create an {@link ObfuscationPropertiesBuilder} by decorating the provided
	 * {@link PropertiesBuilder} with obfuscation functionality.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} to be decorated with
	 *        {@link ObfuscationPropertiesBuilder} functionality.
	 * 
	 * @return The according obfuscating {@link ObfuscationPropertiesBuilder}.
	 */
	public static ObfuscationPropertiesBuilder obfuscate( PropertiesBuilder aProperties ) {
		return new ObfuscationPropertiesBuilderDecorator( aProperties );
	}

	/**
	 * Create an {@link ObfuscationResourcePropertiesBuilder} by decorating the
	 * provided {@link ResourcePropertiesBuilder} with obfuscation
	 * functionality.
	 * 
	 * @param aProperties The {@link ResourcePropertiesBuilder} to be decorated
	 *        with {@link ObfuscationResourcePropertiesBuilder} functionality.
	 * 
	 * @return The according obfuscating
	 *         {@link ObfuscationResourcePropertiesBuilder}.
	 * 
	 * @throws IOException Thrown in case opening the resources for writing back
	 *         encrypted properties failed.
	 */
	public static ObfuscationResourcePropertiesBuilder obfuscate( ResourcePropertiesBuilder aProperties ) throws IOException {
		return new ObfuscationResourcePropertiesBuilderDecorator( aProperties );
	}

	/**
	 * Create an {@link ObfuscationProperties} by decorating the provided
	 * {@link Properties} with obfuscation functionality.
	 * 
	 * @param aProperties The {@link Properties} to be decorated with
	 *        {@link ObfuscationProperties} functionality.
	 * @param aObfuscationMode The {@link SystemContext} specifies which level
	 *        of obfuscation is to be used when encountering upon obfuscated
	 *        properties: E.g. obfuscation may be bound to the host, the
	 *        "secret" used for obfuscation being the same for all applications
	 *        on the same host or obfuscation may be bound to the application,
	 *        being different for different applications on the same host.
	 * 
	 * @return The according obfuscating {@link ObfuscationProperties}.
	 */
	public static ObfuscationProperties obfuscate( Properties aProperties, SystemContext aObfuscationMode ) {
		return new ObfuscationPropertiesDecorator( aProperties, aObfuscationMode );
	}

	/**
	 * Create an {@link ObfuscationPropertiesBuilder} by decorating the provided
	 * {@link PropertiesBuilder} with obfuscation functionality.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} to be decorated with
	 *        {@link ObfuscationPropertiesBuilder} functionality.
	 * @param aObfuscationMode The {@link SystemContext} specifies which level
	 *        of obfuscation is to be used when encountering upon obfuscated
	 *        properties: E.g. obfuscation may be bound to the host, the
	 *        "secret" used for obfuscation being the same for all applications
	 *        on the same host or obfuscation may be bound to the application,
	 *        being different for different applications on the same host.
	 * 
	 * @return The according obfuscating {@link ObfuscationPropertiesBuilder}.
	 */
	public static ObfuscationPropertiesBuilder obfuscate( PropertiesBuilder aProperties, SystemContext aObfuscationMode ) {
		return new ObfuscationPropertiesBuilderDecorator( aProperties, aObfuscationMode );
	}

	/**
	 * Create an {@link ObfuscationResourcePropertiesBuilder} by decorating the
	 * provided {@link ResourcePropertiesBuilder} with obfuscation
	 * functionality.
	 * 
	 * @param aProperties The {@link ResourcePropertiesBuilder} to be decorated
	 *        with {@link ObfuscationResourcePropertiesBuilder} functionality.
	 * @param aObfuscationMode The {@link SystemContext} specifies which level
	 *        of obfuscation is to be used when encountering upon obfuscated
	 *        properties: E.g. obfuscation may be bound to the host, the
	 *        "secret" used for obfuscation being the same for all applications
	 *        on the same host or obfuscation may be bound to the application,
	 *        being different for different applications on the same host.
	 * 
	 * @return The according obfuscating
	 *         {@link ObfuscationResourcePropertiesBuilder}.
	 * 
	 * @throws IOException Thrown in case opening the resources for writing back
	 *         encrypted properties failed.
	 */
	public static ObfuscationResourcePropertiesBuilder obfuscate( ResourcePropertiesBuilder aProperties, SystemContext aObfuscationMode ) throws IOException {
		return new ObfuscationResourcePropertiesBuilderDecorator( aProperties, aObfuscationMode );
	}

	/**
	 * Create an {@link ObfuscationProperties} by decorating the provided
	 * {@link Properties} with obfuscation functionality.
	 * 
	 * @param aProperties The {@link Properties} to be decorated with
	 *        {@link ObfuscationProperties} functionality.
	 * @param aSecret The secret to use when encrypting and decrypting the
	 *        according properties.
	 * 
	 * @return The according obfuscating {@link ObfuscationProperties}.
	 */
	public static ObfuscationProperties obfuscate( Properties aProperties, String aSecret ) {
		return new ObfuscationPropertiesDecorator( aProperties, aSecret );
	}

	/**
	 * Create an {@link ObfuscationPropertiesBuilder} by decorating the provided
	 * {@link PropertiesBuilder} with obfuscation functionality.
	 * 
	 * @param aProperties The {@link PropertiesBuilder} to be decorated with
	 *        {@link ObfuscationPropertiesBuilder} functionality.
	 * @param aSecret The secret to use when encrypting and decrypting the
	 *        according properties.
	 * 
	 * @return The according obfuscating {@link ObfuscationPropertiesBuilder}.
	 */
	public static ObfuscationPropertiesBuilder obfuscate( PropertiesBuilder aProperties, String aSecret ) {
		return new ObfuscationPropertiesBuilderDecorator( aProperties, aSecret );
	}

	/**
	 * Create an {@link ObfuscationResourcePropertiesBuilder} by decorating the
	 * provided {@link ResourcePropertiesBuilder} with obfuscation
	 * functionality.
	 * 
	 * @param aProperties The {@link ResourcePropertiesBuilder} to be decorated
	 *        with {@link ObfuscationResourcePropertiesBuilder} functionality.
	 * @param aSecret The secret to use when encrypting and decrypting the
	 *        according properties.
	 * 
	 * @return The according obfuscating
	 *         {@link ObfuscationResourcePropertiesBuilder}.
	 * 
	 * @throws IOException Thrown in case opening the resources for writing back
	 *         encrypted properties failed.
	 */
	public static ObfuscationResourcePropertiesBuilder obfuscate( ResourcePropertiesBuilder aProperties, String aSecret ) throws IOException {
		return new ObfuscationResourcePropertiesBuilderDecorator( aProperties, aSecret );
	}
}
