// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact extends the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-properties">refcodes-properties</a>
 * toolkit with functionality for <a href=
 * "https://en.wikipedia.org/wiki/Obfuscation_(software))">obfuscating</a>
 * chosen properties in a properties file (also see the blog post <a href=
 * "http://www.refcodes.org/blog/automatically_obfuscate_your_java_applications_configuration">Automatically
 * obfuscate your Java application's configuration</a>).
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-properties"><strong>refcodes-properties:
 * Managing your application's configuration</strong></a> documentation for an
 * up-to-date and detailed description on the usage of this artifact.
 * </p>
 */
package org.refcodes.properties.ext.obfuscation;
