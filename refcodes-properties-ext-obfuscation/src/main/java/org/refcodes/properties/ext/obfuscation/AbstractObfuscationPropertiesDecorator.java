// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import org.refcodes.data.Prefix;
import org.refcodes.properties.AbstractPropertiesDecorator;
import org.refcodes.properties.Properties;

/**
 * Base implementation for a {@link ObfuscationProperties} flavor.
 */
public abstract class AbstractObfuscationPropertiesDecorator extends AbstractPropertiesDecorator<Properties> implements ObfuscationProperties {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _decryptPrefix = Prefix.DECRYPT.getPrefix();
	protected String _encryptPrefix = Prefix.ENCRYPT.getPrefix();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public AbstractObfuscationPropertiesDecorator( Properties aProperties ) {
		super( aProperties );

	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEncryptPrefix() {
		return _encryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDecryptPrefix() {
		return _decryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		String theValue = super.get( aKey );
		if ( theValue != null ) {
			if ( theValue.startsWith( _encryptPrefix ) ) {
				theValue = theValue.substring( _encryptPrefix.length() );
			}
			else if ( theValue.startsWith( _decryptPrefix ) ) {
				theValue = theValue.substring( _decryptPrefix.length() );
				theValue = toDecrypted( theValue );
			}
		}
		return theValue;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decrypts the given value. No prefixing is to be done by this method!
	 * 
	 * @param aValue The value to be decrypted.
	 * 
	 * @return The decrypted value.
	 */
	abstract String toDecrypted( String aValue );

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

}
