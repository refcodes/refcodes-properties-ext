// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import org.refcodes.data.Prefix;
import org.refcodes.exception.BugException;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.properties.Properties;
import org.refcodes.runtime.SystemContext;
import org.refcodes.security.DecryptionException;
import org.refcodes.security.alt.chaos.ChaosKey;
import org.refcodes.security.alt.chaos.ChaosTextDecrypter;
import org.refcodes.textual.SecretHintBuilder;

/**
 * The {@link ObfuscationPropertiesDecorator} represents a decorator for
 * {@link Properties} instances by implementing the
 * {@link ObfuscationProperties} interface. Properties marked in the resource as
 * "to be decrypted" are decrypted (e.g. with a host individual key). Those
 * encrypted properties are decrypted "on-the-fly" (in memory only) upon
 * accessing the according property. Retrieving a value via {@link #get(Object)}
 * (or the like), which is prefixed with "decrypt:" (default), will be decrypted
 * accordingly before passed back to the caller. The prefix may be changed by
 * invoking the according implementaion's constructor. Properties marked in the
 * resource as "to be encrypted" are not modified and returned without the
 * prefix when being queried via {@link #get(Object)} (or the like), as the
 * {@link Properties} are immutable. By default (if not configured otherwise)
 * the prefix {@link Prefix#DECRYPT} is used as decrypt prefix (as of
 * {@link Prefix#getPrefix()}). Also by default (if not configured otherwise)
 * the prefix {@link Prefix#ENCRYPT} is used as encrypt prefix (as of
 * {@link Prefix#getPrefix()}).
 */
public class ObfuscationPropertiesDecorator extends AbstractObfuscationPropertiesDecorator {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ChaosKey _chaosKey;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator. A host specific
	 * secret is used to encrypt and decrypt the according properties.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 */
	public ObfuscationPropertiesDecorator( Properties aProperties ) {
		this( aProperties, SystemContext.HOST.toContextString() );
	}

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator. A host specific
	 * secret is used to encrypt and decrypt the according properties.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 * @param aDecryptPrefix The decrypt prefix to be used for values which are
	 *        to be decrypted, e.g. "decrypt:".
	 * @param aEncryptPrefix The decrypt prefix to be used. for values which are
	 *        to be encrypted, e.g. "encrypt:".
	 */
	public ObfuscationPropertiesDecorator( Properties aProperties, String aDecryptPrefix, String aEncryptPrefix ) {
		this( aProperties, aDecryptPrefix, aEncryptPrefix, SystemContext.HOST.toContextString() );
	}

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator. A host specific
	 * secret is used to encrypt and decrypt the according properties.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 * @param aDecryptPrefix The decrypt prefix to be used for values which are
	 *        to be decrypted, e.g. "decrypt:".
	 * @param aEncryptPrefix The decrypt prefix to be used. for values which are
	 *        to be encrypted, e.g. "encrypt:".
	 * @param aObfuscationMode The {@link SystemContext} specifies which level
	 *        of obfuscation is to be used when encountering upon obfuscated
	 *        properties: E.g. obfuscation may be bound to the host, the
	 *        "secret" used for obfuscation being the same for all applications
	 *        on the same host or obfuscation may be bound to the application,
	 *        being different for different applications on the same host.
	 */
	public ObfuscationPropertiesDecorator( Properties aProperties, String aDecryptPrefix, String aEncryptPrefix, SystemContext aObfuscationMode ) {
		this( aProperties, aDecryptPrefix, aEncryptPrefix, aObfuscationMode.toContextString() );
	}

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator. The given secret
	 * is used to encrypt and decrypt the according properties.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 * @param aDecryptPrefix The decrypt prefix to be used for values which are
	 *        to be decrypted, e.g. "decrypt:".
	 * @param aEncryptPrefix The decrypt prefix to be used. for values which are
	 *        to be encrypted, e.g. "encrypt:".
	 * @param aSecret The secret to use when encrypting and decrypting the
	 *        according properties.
	 */
	public ObfuscationPropertiesDecorator( Properties aProperties, String aDecryptPrefix, String aEncryptPrefix, String aSecret ) {
		super( aProperties );
		final int[] theIds = NumericalUtility.toHashCodes( aSecret, 3 );
		_chaosKey = new ChaosKey( theIds[0], theIds[1], theIds[2] );
		_decryptPrefix = aDecryptPrefix;
		_encryptPrefix = aEncryptPrefix;
	}

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator. A host specific
	 * secret is used to encrypt and decrypt the according properties.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 * @param aObfuscationMode The {@link SystemContext} specifies which level
	 *        of obfuscation is to be used when encountering upon obfuscated
	 *        properties: E.g. obfuscation may be bound to the host, the
	 *        "secret" used for obfuscation being the same for all applications
	 *        on the same host or obfuscation may be bound to the application,
	 *        being different for different applications on the same host.
	 */
	public ObfuscationPropertiesDecorator( Properties aProperties, SystemContext aObfuscationMode ) {
		this( aProperties, aObfuscationMode.toContextString() );
	}

	/**
	 * Decorates the provided {@link Properties} with additional
	 * {@link ObfuscationProperties} behavior or functionality. Changes applied
	 * to the provided {@link Properties} affect the decorator. The given secret
	 * is used to encrypt and decrypt the according properties.
	 * 
	 * @param aProperties The {@link Properties} to be decorated.
	 * @param aSecret The secret to use when encrypting and decrypting the
	 *        according properties.
	 */
	public ObfuscationPropertiesDecorator( Properties aProperties, String aSecret ) {
		super( aProperties );
		final int[] theIds = NumericalUtility.toHashCodes( aSecret, 3 );
		_chaosKey = new ChaosKey( theIds[0], theIds[1], theIds[2] );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	String toDecrypted( String aValue ) {
		try {
			return new ChaosTextDecrypter( _chaosKey ).toDecrypted( aValue );
		}
		catch ( DecryptionException e ) {
			throw new BugException( "Encountered a bug while decrypting value <" + SecretHintBuilder.asString( aValue ) + "> with <" + SystemContext.HOST_USER_APPLICATION_SESSION + "> system context!", e );
		}
	}
}
