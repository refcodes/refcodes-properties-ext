// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.properties.ext.obfuscation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.refcodes.data.Prefix;
import org.refcodes.properties.AbstractResourcePropertiesBuilderDecorator;
import org.refcodes.properties.ResourceProperties;
import org.refcodes.properties.ResourceProperties.ResourcePropertiesBuilder;
import org.refcodes.properties.ext.obfuscation.ObfuscationResourceProperties.ObfuscationResourcePropertiesBuilder;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * Base implementation for a {@link ObfuscationResourcePropertiesBuilder}
 * flavor.
 */
public abstract class AbstractObfuscationResourcePropertiesBuilderDecorator extends AbstractResourcePropertiesBuilderDecorator<ResourcePropertiesBuilder> implements ObfuscationResourcePropertiesBuilder {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _decryptPrefix = Prefix.DECRYPT.getPrefix();
	protected String _encryptPrefix = Prefix.ENCRYPT.getPrefix();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Decorates the provided {@link ResourcePropertiesBuilder} with additional
	 * {@link ObfuscationResourcePropertiesBuilder} behavior or functionality.
	 * Changes applied to the provided {@link ResourcePropertiesBuilder} affect
	 * the decorator.
	 * 
	 * @param aProperties The {@link ResourceProperties} to be decorated.
	 */
	public AbstractObfuscationResourcePropertiesBuilderDecorator( ResourcePropertiesBuilder aProperties ) {
		super( aProperties );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEncryptPrefix() {
		return _encryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEncryptPrefix( String aEncryptPrefix ) {
		_encryptPrefix = aEncryptPrefix;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDecryptPrefix() {
		return _decryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDecryptPrefix( String aDecryptPrefix ) {
		_decryptPrefix = aDecryptPrefix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String get( Object aKey ) {
		String theValue = super.get( aKey );
		if ( theValue != null ) {
			if ( theValue.startsWith( _encryptPrefix ) ) {
				theValue = theValue.substring( _encryptPrefix.length() );
			}
			else if ( theValue.startsWith( _decryptPrefix ) ) {
				theValue = theValue.substring( _decryptPrefix.length() );
				theValue = toDecrypted( theValue );
			}
		}
		return theValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String put( String aKey, String aValue ) {
		if ( aValue != null ) {
			if ( aValue.startsWith( _encryptPrefix ) ) {
				aValue = aValue.substring( _encryptPrefix.length() );
				aValue = toEncrypted( aValue );
				aValue = _decryptPrefix + aValue;
			}
		}
		return super.put( aKey, aValue );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPut( Collection<?> aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPut( Object[] aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPut( Relation<String, String> aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	//	/**
	//	 * {@inheritDoc}
	//	 */
	//	@Override
	//	public ObfuscationResourcePropertiesBuilder withPut( Object aPath, String aValue ) {
	//		put( toPath( aPath ), aValue );
	//		return this;
	//	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPut( Property aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPut( String[] aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObfuscationResourcePropertiesBuilder withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObfuscationResourcePropertiesBuilder withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObfuscationResourcePropertiesBuilder withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObfuscationResourcePropertiesBuilder withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> ObfuscationResourcePropertiesBuilder withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObfuscationResourcePropertiesBuilder withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObfuscationResourcePropertiesBuilder withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObfuscationResourcePropertiesBuilder withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObfuscationResourcePropertiesBuilder withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> ObfuscationResourcePropertiesBuilder withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsert( Object aObj ) {
		insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsert( PathMap<String> aFrom ) {
		insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( Object aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( Object aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( Object aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( Object aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( Object[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( String aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( String aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( String[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMerge( Object aObj ) {
		merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMerge( PathMap<String> aFrom ) {
		merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( Object aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( Object aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( Object aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( Object aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( Object[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( String aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( String[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( int aIndex, Object aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( int aIndex, PathMap<String> aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( String aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withRemoveFrom( Collection<?> aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withRemoveFrom( Object aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withRemoveFrom( Object... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withRemoveFrom( String aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withRemoveFrom( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ObfuscationResourcePropertiesBuilder withRemovePaths( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Encrypts the given value. No prefixing is to be done by this method!
	 * 
	 * @param aValue The value to be encrypted.
	 * 
	 * @return The encrypted value.
	 */
	abstract String toEncrypted( String aValue );

	/**
	 * Decrypts the given value. No prefixing is to be done by this method!
	 * 
	 * @param aValue The value to be decrypted.
	 * 
	 * @return The decrypted value.
	 */
	abstract String toDecrypted( String aValue );

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Encrypts all values prefixed accordingly as returned by
	 * {@link #getEncryptPrefix()}. The properties do not contain any
	 * "to-be-encrypted" values any more, them values then are encrypted. The
	 * changes are only done "in-memory" and are not applied to the resource.
	 * You must {@link #flush()} or otherwise save the properties to make the
	 * changes being reflected in the corresponding resources.
	 * 
	 * @return True when properties have been changed (encrypted) by this
	 *         method.
	 */
	protected boolean encryptAll() {
		boolean isDirty = false;
		String eValue;
		for ( String eKey : keySet() ) {
			eValue = super.get( eKey );
			if ( eValue != null && eValue.startsWith( _encryptPrefix ) ) {
				eValue = eValue.substring( _encryptPrefix.length() );
				eValue = toEncrypted( eValue );
				super.put( eKey, _decryptPrefix + eValue );
				isDirty = true;
			}
		}
		return isDirty;
	}

	/**
	 * Returns a {@link Set} of all the keys which's values are to be encrypted,
	 * e.g. which's values are prefixed with the prefix as returned by
	 * {@link #getEncryptPrefix()}.
	 * 
	 * @return The key {@link Set} of all to be encrypted values.
	 */
	protected Set<String> encryptSet() {
		final Set<String> theResult = new HashSet<>();
		String eValue;
		for ( String eKey : keySet() ) {
			eValue = get( eKey );
			if ( eValue != null && eValue.startsWith( _encryptPrefix ) ) {
				theResult.add( eKey );
			}
		}
		return theResult;
	}
}
