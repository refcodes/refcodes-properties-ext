module org.refcodes.properties.ext.obfuscation {
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires org.refcodes.numerical;
	requires org.refcodes.security;
	requires org.refcodes.security.alt.chaos;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.runtime;
	requires transitive org.refcodes.struct;
	requires org.refcodes.textual;

	exports org.refcodes.properties.ext.obfuscation;
}
